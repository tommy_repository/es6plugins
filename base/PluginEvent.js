class PluginEvent {
    constructor() {

    }

    promise() {
        throw new Error('Method promise is not implemented! Please implement this method and return new Promise()');
    }
}