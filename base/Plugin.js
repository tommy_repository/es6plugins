class Plugin {
    constructor() {
    }

    runInitialization() {
        this._init();
        this._afterInit();
    }
    /**
     * @private
     */
    _init() {}

    /**
     * @private
     */
    _afterInit() {

    }
}