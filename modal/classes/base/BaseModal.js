class BaseModal extends Plugin {
    constructor(element) {
        super();
        this.element = element;
        this.runInitialization();
    }

    init() {}

    open() {
        return BaseModal.beforeOpenEvent().bind(this)()
            .then(BaseModal.openEvent().bind(this))
            .then(BaseModal.afterOpenEvent().bind(this));
    }

    close() {
        return BaseModal.beforeCloseEvent().bind(this)()
            .then(BaseModal.closeEvent().bind(this))
            .then(BaseModal.afterCloseEvent().bind(this));
    }

    /**
     * @return {BeforeOpenEvent.promise}
     */
    static beforeOpenEvent() {
        let beforeOpenEvent = new BeforeOpenEvent();
        return beforeOpenEvent.promise;
    }

    /**
     * @return {BeforeOpenEvent.promise}
     */
    static afterOpenEvent() {
        let afterOpenEvent = new AfterOpenEvent();
        return afterOpenEvent.promise;
    }

    /**
     * @return {BeforeOpenEvent.promise}
     */
    static openEvent() {
        let openEvent = new OpenEvent();
        return openEvent.promise;
    }

    /**
     * @return {BeforeOpenEvent.promise}
     */
    static beforeCloseEvent() {
        let beforeCloseEvent = new BeforeCloseEvent();
        return beforeCloseEvent.promise;
    }

    /**
     * @return {BeforeOpenEvent.promise}
     */
    static closeEvent() {
        let closeEvent = new CloseEvent();
        return closeEvent.promise;
    }

    /**
     * @return {BeforeOpenEvent.promise}
     */
    static afterCloseEvent() {
        let afterCloseEvent = new AfterCloseEvent();
        return afterCloseEvent.promise;
    }
}