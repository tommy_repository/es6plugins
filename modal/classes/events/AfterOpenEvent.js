/**
 *
 */
class AfterOpenEvent extends PluginEvent {

    constructor() {
        super();
    }

    /**
     * @return {Promise}
     */
    promise() {
        return new Promise((resolve, reject) => {
            console.warn('AfterOpenEvent triggered');
            resolve();
        });
    }
}
