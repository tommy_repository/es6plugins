class CloseEvent extends PluginEvent {

    constructor() {
        super();
    }

    /**
     * @return {Promise}
     */
    promise() {
        console.warn('CloseEvent is triggered!');
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                this.element.removeClass('modal-is-active');
                resolve();
            }, 1000)
        });
    }
}