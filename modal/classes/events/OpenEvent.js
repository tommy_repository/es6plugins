class OpenEvent extends PluginEvent {
    constructor() {
        super();
    }

    /**
     * @return {Promise}
     */
    promise() {
        console.warn('OpenEvent promise is triggered!');
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                this.element.addClass('modal-is-active');
                resolve();
            }, 1000);
        });
    }
}