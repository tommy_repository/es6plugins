class BeforeOpenEvent extends PluginEvent {

    constructor() {
        super();
    }

    /**
     * @return {Promise}
     */
    promise() {
        console.warn('BeforeOpenEvent is triggered!');
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve();
            }, 1000)
        });
    }
}