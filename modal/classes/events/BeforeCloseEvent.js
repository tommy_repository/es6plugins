class BeforeCloseEvent extends PluginEvent {

    constructor() {
        super();
    }

    /**
     * @return {Promise}
     */
    promise() {
        console.warn('BeforeCloseEvent is triggered!');
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve();
            }, 1000)
        });
    }
}