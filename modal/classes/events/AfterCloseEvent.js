class AfterCloseEvent extends PluginEvent {

    constructor() {
        super();
    }

    /**
     * @return {Promise}
     */
    promise() {
        console.warn('AfterCloseEvent is triggered!');
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve();
            }, 1000)
        });
    }
}