const gulp = require('gulp');

//load gulp prefixed plugins
const $ = require('gulp-load-plugins')();

//load other plugins
const gutil = require('gulp-util');
const babel = require('gulp-babel');
const es2015 = require('babel-preset-es2015');

const BASE_CLASSES = '../base';
const BASE_BUILD = '../baseDist';
const CLASSES = './classes';
const DIST_FOLDER = './dist';

const PLUGIN_NAME = 'Modal';
const PLUGIN_BASE_NAME = 'BasePlugin';

const babelConfig = {
    presets: [es2015]
};
let paths = {
    // pluginsJS: {
    //     src: [
    //         '../static/js/plugins/**/*',
    //         '../static/js/plugins/*',
    //     ],
    //     build: '../static/js/plugins/',
    // },
    babelJs: {
        baseSrc: [
            BASE_CLASSES + '/**/*',
            BASE_CLASSES + '/*',
        ],
        src: [
            CLASSES + '/**/*',
            CLASSES + '/*',
        ],
        build: DIST_FOLDER + '/babelified',
        baseBuild: BASE_BUILD + '/babelified'
    },
    js: {
        baseSrc: [
            // '../static/js/plugins/plugins.min.js',
            BASE_BUILD + '/babelified/base/*.js',
            BASE_BUILD + '/babelified/**/*',
            BASE_BUILD + '/babelified/*',
        ],
        src: [
            BASE_BUILD + '/babelified/base/*.js',
            BASE_BUILD + '/babelified/*',
            DIST_FOLDER + '/babelified/base/*.js',
            DIST_FOLDER + '/babelified/**/*',
            DIST_FOLDER + '/babelified/*',
        ],
        build: DIST_FOLDER + '/',
        baseBuild: BASE_BUILD + '/',
    },
    scss: {
        src: [
            '../static/scss/*',
            '../static/scss/**/*'
        ],
        build: '../static/dist/'
    },
};

let autoprefixConfig = {
    browsers: ['last 4 versions', 'ie >= 10'],
    cascade: false
};

//error handler
let onError = function (error) {
    //gutil.log(gutil.colors.red(error.message));
    //this.emit('end');
    console.log(error);
    $.notify.onError({
        title: "Gulp Error in " + error.plugin,
        message: "\nError: " + error.message.substr(error.message.indexOf('static'))
    })(error);

    gutil.log();

    this.emit('end');
};

gulp.task('minPluginJs', function () {
    return gulp.src(paths.pluginsJS.src)
        .pipe($.plumber())
        .pipe($.concat('plugins.min.js', {newLine: ''}))
        .pipe($.uglify())
        .pipe(gulp.dest(paths.pluginsJS.build))
});

gulp.task('babelJs', function () {
    return gulp.src(paths.babelJs.src)
        .pipe($.plumber())
        .pipe(babel(babelConfig))
        // .pipe($.concat('es6.js', {newLine: ''}))
        .pipe(gulp.dest(paths.babelJs.build));
});
gulp.task('babelBaseJs', function () {
    return gulp.src(paths.babelJs.baseSrc)
        .pipe($.plumber())
        .pipe(babel(babelConfig))
        // .pipe($.concat('es6.js', {newLine: ''}))
        .pipe(gulp.dest(paths.babelJs.baseBuild));
});

gulp.task('minBaseJs', function () {
    return gulp.src(paths.js.baseSrc)
        .pipe($.plumber())
        .pipe($.concat(PLUGIN_BASE_NAME+'.min.js', {newLine: ''}))
        // .pipe($.uglify())
        .pipe(gulp.dest(paths.js.baseBuild))
});

gulp.task('minJs', function () {
    return gulp.src(paths.js.src)
        .pipe($.plumber())
        .pipe($.concat(PLUGIN_NAME+'.min.js', {newLine: ''}))
        // .pipe($.uglify())
        .pipe($.wrap('var '+PLUGIN_NAME+' = (function(){<%= contents %> ;return '+PLUGIN_NAME+';})();', {}, { parse: false }))
        .pipe(gulp.dest(paths.js.build))
});

gulp.task('minCss', function () {
    return gulp.src(paths.scss.src)
        .pipe($.plumber({errorHandler: onError}))
        .pipe($.sass())
        .pipe($.combineMq())
        .pipe($.autoprefixer(autoprefixConfig))
        .pipe($.cssnano())
        .pipe($.concat('style.min.css'))
        .pipe(gulp.dest(paths.scss.build))
});

/**
 * Watchers
 */
gulp.task('allWatcher', ['babelJs', 'minJs', 'minCss'], function () {
    gulp.watch(paths.babelJs.src, ['babelJs']);
    gulp.watch(paths.js.src, ['minJs']);
    gulp.watch(paths.scss.src, ['minCss']);
});
gulp.task('jsWatcher', ['babelJs'], function () {
    gulp.watch(paths.babelJs.src, ['babelJs']);
    gulp.watch(paths.js.src, ['minJs']);
});