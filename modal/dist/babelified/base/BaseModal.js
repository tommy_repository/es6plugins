"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var BaseModal = function (_Plugin) {
    _inherits(BaseModal, _Plugin);

    function BaseModal(element) {
        _classCallCheck(this, BaseModal);

        var _this = _possibleConstructorReturn(this, (BaseModal.__proto__ || Object.getPrototypeOf(BaseModal)).call(this));

        _this.element = element;
        _this.runInitialization();
        return _this;
    }

    _createClass(BaseModal, [{
        key: "init",
        value: function init() {}
    }, {
        key: "open",
        value: function open() {
            return BaseModal.beforeOpenEvent().bind(this)().then(BaseModal.openEvent().bind(this)).then(BaseModal.afterOpenEvent().bind(this));
        }
    }, {
        key: "close",
        value: function close() {
            return BaseModal.beforeCloseEvent().bind(this)().then(BaseModal.closeEvent().bind(this)).then(BaseModal.afterCloseEvent().bind(this));
        }

        /**
         * @return {BeforeOpenEvent.promise}
         */

    }], [{
        key: "beforeOpenEvent",
        value: function beforeOpenEvent() {
            var beforeOpenEvent = new BeforeOpenEvent();
            return beforeOpenEvent.promise;
        }

        /**
         * @return {BeforeOpenEvent.promise}
         */

    }, {
        key: "afterOpenEvent",
        value: function afterOpenEvent() {
            var afterOpenEvent = new AfterOpenEvent();
            return afterOpenEvent.promise;
        }

        /**
         * @return {BeforeOpenEvent.promise}
         */

    }, {
        key: "openEvent",
        value: function openEvent() {
            var openEvent = new OpenEvent();
            return openEvent.promise;
        }

        /**
         * @return {BeforeOpenEvent.promise}
         */

    }, {
        key: "beforeCloseEvent",
        value: function beforeCloseEvent() {
            var beforeCloseEvent = new BeforeCloseEvent();
            return beforeCloseEvent.promise;
        }

        /**
         * @return {BeforeOpenEvent.promise}
         */

    }, {
        key: "closeEvent",
        value: function closeEvent() {
            var closeEvent = new CloseEvent();
            return closeEvent.promise;
        }

        /**
         * @return {BeforeOpenEvent.promise}
         */

    }, {
        key: "afterCloseEvent",
        value: function afterCloseEvent() {
            var afterCloseEvent = new AfterCloseEvent();
            return afterCloseEvent.promise;
        }
    }]);

    return BaseModal;
}(Plugin);